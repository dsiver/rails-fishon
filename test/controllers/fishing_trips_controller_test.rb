require 'test_helper'

class FishingTripsControllerTest < ActionController::TestCase
  setup do
    @fishing_trip = fishing_trips(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:fishing_trips)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create fishing_trip" do
    assert_difference('FishingTrip.count') do
      post :create, fishing_trip: {  }
    end

    assert_redirected_to fishing_trip_path(assigns(:fishing_trip))
  end

  test "should show fishing_trip" do
    get :show, id: @fishing_trip
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @fishing_trip
    assert_response :success
  end

  test "should update fishing_trip" do
    patch :update, id: @fishing_trip, fishing_trip: {  }
    assert_redirected_to fishing_trip_path(assigns(:fishing_trip))
  end

  test "should destroy fishing_trip" do
    assert_difference('FishingTrip.count', -1) do
      delete :destroy, id: @fishing_trip
    end

    assert_redirected_to fishing_trips_path
  end
end
