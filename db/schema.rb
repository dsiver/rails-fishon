# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150701192929) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "fish", force: :cascade do |t|
    t.integer  "species_id"
    t.integer  "member_id"
    t.integer  "fishing_trip_id"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "ipl",             default: 0
    t.float    "fpl",             default: 0.0
  end

  add_index "fish", ["fishing_trip_id"], name: "index_fish_on_fishing_trip_id", using: :btree
  add_index "fish", ["member_id"], name: "index_fish_on_member_id", using: :btree
  add_index "fish", ["species_id"], name: "index_fish_on_species_id", using: :btree

  create_table "fishing_trip_members", force: :cascade do |t|
    t.integer  "fishing_trip_id"
    t.integer  "member_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "fishing_trip_members", ["fishing_trip_id"], name: "index_fishing_trip_members_on_fishing_trip_id", using: :btree
  add_index "fishing_trip_members", ["member_id"], name: "index_fishing_trip_members_on_member_id", using: :btree

  create_table "fishing_trips", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "name"
  end

  create_table "members", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "password_digest"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.boolean  "active",          default: true
  end

  create_table "species", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "fish", "fishing_trips"
  add_foreign_key "fish", "members"
  add_foreign_key "fish", "species"
  add_foreign_key "fishing_trip_members", "fishing_trips"
  add_foreign_key "fishing_trip_members", "members"
end
