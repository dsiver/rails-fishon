Member.create([
                  {first_name: "David, Jr.", last_name: "Siver", email: "davidsiver@gmail.com",
                   password: "password", password_confirmation: "password", active: true},
                  {first_name: "David, Sr.", last_name: "Siver", email: "dave57231@gmail.com",
                   password: "password", password_confirmation: "password", active: true}
              ])
Species.create([
                   {name: "Walleye/Sauger"},
                   {name: "Perch"},
                   {name: "Northern Pike"},
                   {name: "Crappie"},
                   {name: "Smallmouth Bass"},
                   {name: "Sturgeon"},
                   {name: "Trout"},
                   {name: "Gar"}
               ])