class AddIplFplToFish < ActiveRecord::Migration
  def change
    add_column :fish, :ipl, :integer, default: 0
    add_column :fish, :fpl, :float, default: 0
  end
end
