class CreateFish < ActiveRecord::Migration
  def change
    create_table :fish do |t|
      t.references :species, index: true
      t.references :member, index: true
      t.references :fishing_trip, index: true
      t.float :length

      t.timestamps null: false
    end
    add_foreign_key :fish, :species
    add_foreign_key :fish, :members
    add_foreign_key :fish, :fishing_trips
  end
end
