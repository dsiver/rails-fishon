class CreateFishingTripMembers < ActiveRecord::Migration
  def change
    create_table :fishing_trip_members do |t|
      t.references :fishing_trip, index: true
      t.references :member, index: true

      t.timestamps null: false
    end
    add_foreign_key :fishing_trip_members, :fishing_trips
    add_foreign_key :fishing_trip_members, :members
  end
end
