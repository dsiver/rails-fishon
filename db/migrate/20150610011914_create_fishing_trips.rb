class CreateFishingTrips < ActiveRecord::Migration
  def change
    create_table :fishing_trips do |t|

      t.timestamps null: false
    end
  end
end
