class AddNameToFishingTrip < ActiveRecord::Migration
  def change
    add_column :fishing_trips, :name, :string
  end
end
