class RemoveLengthFromFish < ActiveRecord::Migration
  def change
    remove_column :fish, :length, :float
  end
end
