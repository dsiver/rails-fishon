class FishingTripMembersController < ApplicationController

  def member_fishing_trips
    @member = Member.find(params[:id])
    @fishing_trips = @member.fishing_trips
  end

  def member_fishing_trip
    @fishing_trip = FishingTrip.find(params[:id])
    @members = @fishing_trip.members
  end

end
