class FishingTripsController < ApplicationController
  before_action :set_fishing_trip, only: [:show, :edit, :update, :destroy]

  # GET /fishing_trips
  # GET /fishing_trips.json
  def index
    @fishing_trips = FishingTrip.all
  end

  # GET /fishing_trips/1
  # GET /fishing_trips/1.json
  def show
    @fishing_trip = FishingTrip.find(params[:id])
    @members = @fishing_trip.members
  end

  # GET /fishing_trips/new
  def new
    @members = Member.all.order(last_name: :asc)
    @fishing_trip = FishingTrip.new
  end

  # GET /fishing_trips/1/edit
  def edit
    @fishing_trip = FishingTrip.find(params[:id])
    @members = @fishing_trip.members
  end

  # POST /fishing_trips
  # POST /fishing_trips.json
  def create
    @fishing_trip = FishingTrip.new(fishing_trip_params)

    respond_to do |format|
      if @fishing_trip.save
        format.html { redirect_to root_path, notice: 'Fishing trip was successfully created.' }
        format.json { render :show, status: :created, location: @fishing_trip }
      else
        format.html { render :new }
        format.json { render json: @fishing_trip.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /fishing_trips/1
  # PATCH/PUT /fishing_trips/1.json
  def update
    respond_to do |format|
      if @fishing_trip.update(fishing_trip_params)
        format.html { redirect_to @fishing_trip, notice: 'Fishing trip was successfully updated.' }
        format.json { render :show, status: :ok, location: @fishing_trip }
      else
        format.html { render :edit }
        format.json { render json: @fishing_trip.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fishing_trips/1
  # DELETE /fishing_trips/1.json
  def destroy
    @fishing_trip.destroy
    respond_to do |format|
      format.html { redirect_to fishing_trips_url, notice: 'Fishing trip was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_fishing_trip
    @fishing_trip = FishingTrip.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def fishing_trip_params
    params.require(:fishing_trip).permit(:id, :name, :member_ids => [])
  end
end
