json.array!(@fishing_trips) do |fishing_trip|
  json.extract! fishing_trip, :id
  json.url fishing_trip_url(fishing_trip, format: :json)
end
