json.array!(@fish) do |fish|
  json.extract! fish, :id, :species_id, :member_id, :fishing_trip_id, :length
  json.url fish_url(fish, format: :json)
end
