class Member < ActiveRecord::Base
  #has_secure_password
  has_many :fish
  has_many :fishing_trip_members
  has_many :fishing_trips, through: :fishing_trip_members
  accepts_nested_attributes_for :fishing_trips
  accepts_nested_attributes_for :fishing_trip_members

  validates_presence_of :first_name, :last_name#, :email

  def self.available_members(user_id)
    Member.where.not(id: user_id).where(active: true)
  end

  def self.active_members
    Member.where(active: true)
  end

  def self.members_map
    Member.all.order(last_name: :asc).map{ |member| [ member.formatted_name, member.id ] }
  end

  def formatted_name
    @name = self.last_name + ', ' + self.first_name
  end

  def species_caught_ids
    @species_ids = self.fish.pluck(:species_id)
  end

  def species_caught_names
    @species_names = Species.where(id: self.species_caught_ids).pluck(:name)
  end

  def species_count(species_id)
    self.fish.where(species_id: species_id).count
  end

  def species_count_hash
    species_hash = Hash.new
    species_caught_ids.each do |id|
      species_hash[Species.find(id)] = species_count(id)
    end
    species_hash
  end

end
