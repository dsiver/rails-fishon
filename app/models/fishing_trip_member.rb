class FishingTripMember < ActiveRecord::Base
  belongs_to :fishing_trip
  belongs_to :member
end
