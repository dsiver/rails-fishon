class FishingTrip < ActiveRecord::Base
  has_many :fishing_trip_members
  has_many :members, through: :fishing_trip_members
  accepts_nested_attributes_for :members
  accepts_nested_attributes_for :fishing_trip_members

  validates_presence_of :members, :name
  validates_uniqueness_of :name
end
