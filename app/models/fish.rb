class Fish < ActiveRecord::Base
  belongs_to :species
  belongs_to :member
  belongs_to :fishing_trip

  validates_presence_of :ipl, :fpl, :species, :member, :fishing_trip
  validates :ipl, numericality: { greater_than_or_equal_to: 0 }

  def length
    MathUtils.combine_length(self.ipl, self.fpl)
  end
end
