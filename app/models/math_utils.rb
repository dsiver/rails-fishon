class MathUtils
  include ActiveModel::Model

  def self.fish_length_decimals
    @fish_length_decimals = { 0 => 0.00, Rational(1, 4) => 0.25, Rational(1, 2) => 0.50, Rational(3, 4) => 0.75 }
  end

  def self.combine_length(number, decimal)
    number.to_f + decimal.to_f
  end

end