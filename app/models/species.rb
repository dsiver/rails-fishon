class Species < ActiveRecord::Base
  has_many :fish

  validates_presence_of :name

  def self.species_map
    Species.all.order(name: :asc).map{ |species| [ species.name, species.id ] }
  end

  def self.name_by_id(id)
    Species.find(id).name
  end

end
